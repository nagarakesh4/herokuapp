var express = require('express');
var router = express.Router();
var appdata = require('../data.json');
var Firebase = require("firebase");
var myFirebaseRef = new Firebase("https://nodejsapp.firebaseio.com/");

/* GET home page. */
router.get('/', function(req, res) {
	errorMessage = "";
	res.render('login', {
	title : 'User Login',
	pageName : 'Login'
	});
});
router.get('/login', function(req, res) {
	errorMessage = "";
	res.render('login', {
	title : 'User Login',
	pageName : 'Login'
	});
});

router.post('/validateUser', function(req, res) {
	myFirebaseRef.authWithPassword({
	  email    : req.body.username,
	  password : req.body.password
	}, function(error, authData) {
	  if (error) {
	    res.render('login', {
		title : 'Login',
		pageName : 'Login',
		errorMessage : "Login Failed, try again"
		});
	  } else {
		  var myArtwork = [];
		  var myArtists = [];
		  myArtists = appdata.speakers;
		  appdata.speakers.forEach(function(item) {
			myArtwork = myArtwork.concat(item.artwork);
		  });
		  res.render('index', {
			title: 'Home',
			username : authData.password.email,
			artwork: myArtwork,
			artists: myArtists
		  });
		}
	  }); 
});

router.get('/signUp', function(req, res) {
	errorMessage = "";
	color= "";
	res.render('register', {
		title: 'User Registration',
		pageName : 'Register Here'
	});
});

router.post('/registerUser', function(req, res){
	myFirebaseRef.createUser({
	  email    : req.body.username,
	  password : req.body.password
	}, function(error, userData) {
	  if (error) {
		console.log(error);
		res.render('register', {
		title : 'Register',
		pageName : 'Register Here',
		errorMessage : "Registration Failed. "+error,
		color: "red"
		});
	  } else {
		console.log(userData);
		res.render('register', {
		title : 'Register',
		pageName : 'Register Here',
		errorMessage : "Registration Successful, you can now login with "+req.body.username,
		color : "green"
		});
	  }
	});
});

router.get('/home', function(req, res) {
  var myArtwork = [];
  var myArtists = [];
  if(req.body.username!=null)
	username = req.body.username;
  else
	username = "Login to know you ";
  myArtists = appdata.speakers;
  appdata.speakers.forEach(function(item) {
    myArtwork = myArtwork.concat(item.artwork);
  });
  res.render('index', { 
    title: 'Home',
    artwork: myArtwork,
    artists: myArtists
  });
});


router.get('/facebookLogin', function(req, res) {
	myFirebaseRef.authWithOAuthPopup("facebook", function(error, authData) {
	  if (error) {
		console.log("Login Failed!", error);
	  } else {
		console.log("Authenticated successfully with payload:", authData);
	  }
	});
});

/* GET speakers page. */
router.get('/speakers', function(req, res) {
  var myArtwork = [];
  var myArtists = [];
  myArtists = appdata.speakers;

  appdata.speakers.forEach(function(item) {
    myArtwork = myArtwork.concat(item.artwork);
  });
  res.render('speakers', {
    title: 'Speakers',
    artwork: myArtwork,
    artists: myArtists
  });
});


/* GET speakers page. */
router.get('/speakers/:speakerid', function(req, res) {

  var myArtwork = [];
  var myArtists = [];

  appdata.speakers.forEach(function(item) {
    if (item.shortname == req.params.speakerid) {
      myArtists.push(item);
      myArtwork = myArtwork.concat(item.artwork);
    }
  });
  res.render('speakers', {
    title: 'Speakers',
    artwork: myArtwork,
    artists: myArtists
  });
});

module.exports = router;
